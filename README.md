1. Run Lattice inference.ipynb to produce lattices for different energy deltas
2. Run Get phase data.ipynb to run the experiments on the lattices' stability under different temperatures
3. Run Analyze results.ipynb to produce phase-T plots
